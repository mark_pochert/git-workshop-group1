#!/usr/bin/env bash
set -o errexit -o noclobber -o nounset -o pipefail

# Wenn kein Name gesetzt, dann World
echo "Hello ${1:-World}"
